import { Component, EventEmitter, Output, OnInit, Inject, forwardRef} from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  // tslint:disable-next-line: no-output-on-prefix
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minlongitud: number;
  searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig)  {
    this.onItemAdded = new EventEmitter();
    this.minlongitud = 3;
    this.fg = fb.group ({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator(this.minlongitud)
      ])],
      descripcion: ['', Validators.nullValidator],
      url: ['']
    });

    this.fg.valueChanges.subscribe((forms: any) => {
      console.log('cambio el formulario: ', forms);
    });
   }

  ngOnInit() {
    // tslint:disable-next-line: no-angle-bracket-type-assertion
    const elemNombre = <HTMLInputElement> document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
    ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar( nombre: string, descripcion: string, url: string): boolean {
    const d = new DestinoViaje(nombre, descripcion, url);
    console.log('Entro');
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minlongNombre: true };
      }
      return null;
    };
  }



}

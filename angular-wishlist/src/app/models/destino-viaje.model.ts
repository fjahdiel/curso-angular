import { v4 as uuid } from 'uuid';
export class DestinoViaje {
  private selected: boolean;
  public servicios: string[];
  id = uuid();
  public votes = 0;
    constructor(  public nombre: string, public descripcion: string, public url: string) {
      this.servicios = ['desayuno', 'piscina'];
     }
  setSelected(s: boolean) {
    this.selected = s;
  }
  isSelected() {
    return this.selected;
  }
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}

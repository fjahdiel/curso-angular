import { DestinoViaje } from './destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState, db, MyDatabase  } from '../app.module';
import {
  NuevoDestinoAction,
  ElegidoFavoritoAction
} from './destinos-viajes-state.model';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { APP_CONFIG, AppConfig } from './../app.module';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];

   constructor(
     private store: Store<AppState>,
     @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
     private http: HttpClient
   ) {

   }

  add(d: DestinoViaje) {
    const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
    // tslint:disable-next-line: object-literal-shorthand
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(d));
        const myDb = db;
        myDb.destinos.add(d);
        console.log('todos los destinos de la db!');
        myDb.destinos.toArray().then(destinos => console.log(destinos));
      }
    });
   }

    elegir(d: DestinoViaje) {
      this.store.dispatch(new ElegidoFavoritoAction(d));
    }

}
